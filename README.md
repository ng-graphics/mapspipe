# README #

Starter shell to test feasibility of sharing virtual production environment to create digital maps (as vagrant box)


### How do I get set up? ###

FIRST --> Download and install [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/)

THEN -->

`git clone https://ng-graphics@bitbucket.org/ng-graphics/mapspipe.git mapspipe`

`cd mapspipe`

`vagrant box add mapspipe mapspipe.box`

`vagrant init mapspipe`

`vagrant up`

`vagrant ssh` if prompted, password is `vagrant`

If it's working, you should see a welcome screen in your terminal.. that's it for now
